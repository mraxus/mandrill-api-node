# README #

This is a modified clone of the Mandrill nodeJS API.
I have modified it so it would handle "normal" callbacks with (err, res) instead of the double callback(hell) that the official API uses.

The conversion is mainly for the messages routes. On other words, I have not converted the complete repo.

Please look into the source for details of how to use it, as well as the official documentation:

https://mandrillapp.com/api/docs/index.nodejs.html

//Cheers