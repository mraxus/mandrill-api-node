
https = require 'https'
version = require('./package.json').version

OPTS =
    host:   'mandrillapp.com',
    port:   443,
    prefix: '/api/1.0/',
    method: 'POST',
    headers:
        'Content-Type': 'application/json'
        'User-Agent': 'Mandrill-Node/' + version

Exports = require './lib/exports'
Inbound = require './lib/inbound'
Ips = require './lib/ips'
Messages = require './lib/messages'
Metadata = require './lib/metadata'
Rejects = require './lib/rejects'
Senders = require './lib/senders'
Subaccounts = require './lib/subaccounts'
Tags = require './lib/tags'
Templates = require './lib/templates'
Urls = require './lib/urls'
Users = require './lib/users'
Webhooks = require './lib/webhooks'
Whitelists = require './lib/whitelists'

class exports.Mandrill
    constructor: (@apikey=null, @debug=false) ->
        @templates = new Templates(this)
        @exports = new Exports(this)
        @users = new Users(this)
        @rejects = new Rejects(this)
        @inbound = new Inbound(this)
        @tags = new Tags(this)
        @messages = new Messages(this)
        @whitelists = new Whitelists(this)
        @ips = new Ips(this)
        @internal = new Internal(this)
        @subaccounts = new Subaccounts(this)
        @urls = new Urls(this)
        @webhooks = new Webhooks(this)
        @senders = new Senders(this)
        @metadata = new Metadata(this)

        if @apikey == null then @apikey = process.env['MANDRILL_APIKEY']

    call: (uri, params={}, callback) ->
        params.key = @apikey
        params = new Buffer(JSON.stringify(params), 'utf8')

        if @debug then console.log("Mandrill: Opening request to https://#{OPTS.host}#{OPTS.prefix}#{uri}.json")
        OPTS.path = "#{OPTS.prefix}#{uri}.json"
        OPTS.headers['Content-Length'] = params.length
        req = https.request(OPTS, (res) =>
            res.setEncoding('utf8')
            data = ''
            res.on('data', (d) =>
                data += d
            )

            res.on('end', =>

                err = null
                json = null

                try
                    json = JSON.parse(data)
                catch e
                    err = {status: 'error', name: 'GeneralError', message: e}

                if err == null && res.statusCode != 200
                    err = json
                    json = null

                if err && !callback then @onerror(json)
                else callback(err, json)
            )
        )
        req.write(params)
        req.end()
        req.on('error', (e) =>
            if callback then callback(e) else @onerror({status: 'error', name: 'GeneralError', message: e})
        )

        return null

    onerror: (err) ->
        throw {name: err.name, message: err.message, toString: -> "#{err.name}: #{err.message}"}

class Internal
    constructor: (@master) ->
