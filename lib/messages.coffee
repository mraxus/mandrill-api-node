expandTemplateContent = (params) ->
    return null if not params.template_content
    expanded = []
    for name, content of params.template_content
        expanded.push { name: name, content: content }

    params.template_content = expanded
    return

expandNameContent = (mergeVars) ->
    return null if not mergeVars
    { name: name, content: mergeVars[name] } for name in Object.keys(mergeVars)

expandMessageMergeVars = (message) ->
    return if not message
    message.global_merge_vars = expandNameContent(message.global_merge_vars)
    if message && message.merge_vars
        for item in message.merge_vars
            item.vars = expandNameContent(item.vars)
    return

module.exports = class Messages
    constructor: (@master) ->

    ###
    Send a new transactional message through Mandrill
    @param {Object} params the hash of the parameters to pass to the request
    @option params {Struct} message the information on the message to send
         - html {String} the full HTML content to be sent
         - text {String} optional full text content to be sent
         - subject {String} the message subject
         - from_email {String} the sender email address.
         - from_name {String} optional from name to be used
         - to {Array} an array of recipient information.
             - to[] {Object} a single recipient's information.
                 - email {String} the email address of the recipient
                 - name {String} the optional display name to use for the recipient
                 - type {String} the header type to use for the recipient, defaults to "to" if not provided
         - headers {Object} optional extra headers to add to the message (most headers are allowed)
         - important {Boolean} whether or not this message is important, and should be delivered ahead of non-important messages
         - track_opens {Boolean} whether or not to turn on open tracking for the message
         - track_clicks {Boolean} whether or not to turn on click tracking for the message
         - auto_text {Boolean} whether or not to automatically generate a text part for messages that are not given text
         - auto_html {Boolean} whether or not to automatically generate an HTML part for messages that are not given HTML
         - inline_css {Boolean} whether or not to automatically inline all CSS styles provided in the message HTML - only for HTML documents less than 256KB in size
         - url_strip_qs {Boolean} whether or not to strip the query string from URLs when aggregating tracked URL data
         - preserve_recipients {Boolean} whether or not to expose all recipients in to "To" header for each email
         - view_content_link {Boolean} set to false to remove content logging for sensitive emails
         - bcc_address {String} an optional address to receive an exact copy of each recipient's email
         - tracking_domain {String} a custom domain to use for tracking opens and clicks instead of mandrillapp.com
         - signing_domain {String} a custom domain to use for SPF/DKIM signing instead of mandrill (for "via" or "on behalf of" in email clients)
         - return_path_domain {String} a custom domain to use for the messages's return-path
         - merge {Boolean} whether to evaluate merge tags in the message. Will automatically be set to true if either merge_vars or global_merge_vars are provided.
         - global_merge_vars {Object} global merge variables to use for all recipients. You can override these per recipient.
             - [key] {String} the global merge variable's name. Merge variable names are case-insensitive and may not start with _
             - [value] {String} the global merge variable's content
         - merge_vars {Array} per-recipient merge variables, which override global merge variables with the same name.
             - merge_var {Object} per-recipient merge variables
                 - rcpt {String} the email address of the recipient that the merge variables should apply to
                 - vars {Object} the recipient's merge variables
                     - [key] {String} the global merge variable's name. Merge variable names are case-insensitive and may not start with _
                     - [value] {String} the global merge variable's content
         - tags {Array} an array of string to tag the message with.  Stats are accumulated using tags, though we only store the first 100 we see, so this should not be unique or change frequently.  Tags should be 50 characters or less.  Any tags starting with an underscore are reserved for internal use and will cause errors.
             - tags[] {String} a single tag - must not start with an underscore
         - subaccount {String} the unique id of a subaccount for this message - must already exist or will fail with an error
         - google_analytics_domains {Array} an array of strings indicating for which any matching URLs will automatically have Google Analytics parameters appended to their query string automatically.
         - google_analytics_campaign {Array|string} optional string indicating the value to set for the utm_campaign tracking parameter. If this isn't provided the email's from address will be used instead.
         - metadata {Array} metadata an associative array of user metadata. Mandrill will store this metadata and make it available for retrieval. In addition, you can select up to 10 metadata fields to index and make searchable using the Mandrill search api.
         - recipient_metadata {Array} Per-recipient metadata that will override the global values specified in the metadata parameter.
             - recipient_metadata[] {Object} metadata for a single recipient
                 - rcpt {String} the email address of the recipient that the metadata is associated with
                 - values {Array} an associated array containing the recipient's unique metadata. If a key exists in both the per-recipient metadata and the global metadata, the per-recipient metadata will be used.
         - attachments {Array} an array of supported attachments to add to the message
             - attachments[] {Object} a single supported attachment
                 - type {String} the MIME type of the attachment
                 - name {String} the file name of the attachment
                 - content {String} the content of the attachment as a base64-encoded string
         - images {Array} an array of embedded images to add to the message
             - images[] {Object} a single embedded image
                 - type {String} the MIME type of the image - must start with "image/"
                 - name {String} the Content ID of the image - use <img src="cid:THIS_VALUE"> to reference the image in your HTML content
                 - content {String} the content of the image as a base64-encoded string
    @option params {Boolean} async enable a background sending mode that is optimized for bulk sending. In async mode, messages/send will immediately return a status of "queued" for every recipient. To handle rejections when sending in async mode, set up a webhook for the 'reject' event. Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are always sent asynchronously, regardless of the value of async.
    @option params {String} ip_pool the name of the dedicated ip pool that should be used to send the message. If you do not have any dedicated IPs, this parameter has no effect. If you specify a pool that does not exist, your default pool will be used instead.
    @option params {String} send_at when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. If you specify a time in the past, the message will be sent immediately. An additional fee applies for scheduled email, and this feature is only available to accounts with a positive balance.
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    send: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["async"] ?= false
        params["ip_pool"] ?= null
        params["send_at"] ?= null

        expandMessageMergeVars params.message

        @master.call('messages/send', params, callback)

    ###
    Send a new transactional message through Mandrill using a template
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} template_name the immutable name or slug of a template that exists in the user's account. For backwards-compatibility, the template name may also be used but the immutable slug is preferred.
    @option params {Object} template_content a key/value object of template content to send: [key]: the name of the content block to set the content for, and [value]: the actual content to put into the block
         - [key] {String} the name of the mc:edit editable region to inject into
         - [value] {String} the content to inject
    @option params {Struct} message the other information on the message to send - same as /messages/send, but without the html content
         - html {String} optional full HTML content to be sent if not in template
         - text {String} optional full text content to be sent
         - subject {String} the message subject
         - from_email {String} the sender email address.
         - from_name {String} optional from name to be used
         - to {Array} an array of recipient information.
             - to[] {Object} a single recipient's information.
                 - email {String} the email address of the recipient
                 - name {String} the optional display name to use for the recipient
                 - type {String} the header type to use for the recipient, defaults to "to" if not provided
         - headers {Object} optional extra headers to add to the message (most headers are allowed)
         - important {Boolean} whether or not this message is important, and should be delivered ahead of non-important messages
         - track_opens {Boolean} whether or not to turn on open tracking for the message
         - track_clicks {Boolean} whether or not to turn on click tracking for the message
         - auto_text {Boolean} whether or not to automatically generate a text part for messages that are not given text
         - auto_html {Boolean} whether or not to automatically generate an HTML part for messages that are not given HTML
         - inline_css {Boolean} whether or not to automatically inline all CSS styles provided in the message HTML - only for HTML documents less than 256KB in size
         - url_strip_qs {Boolean} whether or not to strip the query string from URLs when aggregating tracked URL data
         - preserve_recipients {Boolean} whether or not to expose all recipients in to "To" header for each email
         - view_content_link {Boolean} set to false to remove content logging for sensitive emails
         - bcc_address {String} an optional address to receive an exact copy of each recipient's email
         - tracking_domain {String} a custom domain to use for tracking opens and clicks instead of mandrillapp.com
         - signing_domain {String} a custom domain to use for SPF/DKIM signing instead of mandrill (for "via" or "on behalf of" in email clients)
         - return_path_domain {String} a custom domain to use for the messages's return-path
         - merge {Boolean} whether to evaluate merge tags in the message. Will automatically be set to true if either merge_vars or global_merge_vars are provided.
         - global_merge_vars {Object} global merge variables to use for all recipients. You can override these per recipient.
             - [key] {String} the global merge variable's name. Merge variable names are case-insensitive and may not start with _
             - [value] {String} the global merge variable's content
         - merge_vars {Array} per-recipient merge variables, which override global merge variables with the same name.
             - merge_var {Object} per-recipient merge variables
                 - rcpt {String} the email address of the recipient that the merge variables should apply to
                 - vars {Object} the recipient's merge variables
                     - [key] {String} the global merge variable's name. Merge variable names are case-insensitive and may not start with _
                     - [value] {String} the global merge variable's content
         - tags {Array} an array of string to tag the message with.  Stats are accumulated using tags, though we only store the first 100 we see, so this should not be unique or change frequently.  Tags should be 50 characters or less.  Any tags starting with an underscore are reserved for internal use and will cause errors.
             - tags[] {String} a single tag - must not start with an underscore
         - subaccount {String} the unique id of a subaccount for this message - must already exist or will fail with an error
         - google_analytics_domains {Array} an array of strings indicating for which any matching URLs will automatically have Google Analytics parameters appended to their query string automatically.
         - google_analytics_campaign {Array|string} optional string indicating the value to set for the utm_campaign tracking parameter. If this isn't provided the email's from address will be used instead.
         - metadata {Array} metadata an associative array of user metadata. Mandrill will store this metadata and make it available for retrieval. In addition, you can select up to 10 metadata fields to index and make searchable using the Mandrill search api.
         - recipient_metadata {Array} Per-recipient metadata that will override the global values specified in the metadata parameter.
             - recipient_metadata[] {Object} metadata for a single recipient
                 - rcpt {String} the email address of the recipient that the metadata is associated with
                 - values {Array} an associated array containing the recipient's unique metadata. If a key exists in both the per-recipient metadata and the global metadata, the per-recipient metadata will be used.
         - attachments {Array} an array of supported attachments to add to the message
             - attachments[] {Object} a single supported attachment
                 - type {String} the MIME type of the attachment
                 - name {String} the file name of the attachment
                 - content {String} the content of the attachment as a base64-encoded string
         - images {Array} an array of embedded images to add to the message
             - images[] {Object} a single embedded image
                 - type {String} the MIME type of the image - must start with "image/"
                 - name {String} the Content ID of the image - use <img src="cid:THIS_VALUE"> to reference the image in your HTML content
                 - content {String} the content of the image as a base64-encoded string
    @option params {Boolean} async enable a background sending mode that is optimized for bulk sending. In async mode, messages/send will immediately return a status of "queued" for every recipient. To handle rejections when sending in async mode, set up a webhook for the 'reject' event. Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are always sent asynchronously, regardless of the value of async.
    @option params {String} ip_pool the name of the dedicated ip pool that should be used to send the message. If you do not have any dedicated IPs, this parameter has no effect. If you specify a pool that does not exist, your default pool will be used instead.
    @option params {String} send_at when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. If you specify a time in the past, the message will be sent immediately. An additional fee applies for scheduled email, and this feature is only available to accounts with a positive balance.
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    sendTemplate: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["async"] ?= false
        params["ip_pool"] ?= null
        params["send_at"] ?= null

        expandTemplateContent params
        expandMessageMergeVars params.message

        @master.call('messages/send-template', params, callback)

    ###
    Search the content of recently sent messages and optionally narrow by date range, tags and senders
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} query the search terms to find matching messages for
    @option params {String} date_from start date
    @option params {String} date_to end date
    @option params {Array} tags an array of tag names to narrow the search to, will return messages that contain ANY of the tags
    @option params {Array} senders an array of sender addresses to narrow the search to, will return messages sent by ANY of the senders
    @option params {Array} api_keys an array of API keys to narrow the search to, will return messages sent by ANY of the keys
    @option params {Integer} limit the maximum number of results to return, defaults to 100, 1000 is the maximum
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    search: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["query"] ?= '*'
        params["date_from"] ?= null
        params["date_to"] ?= null
        params["tags"] ?= null
        params["senders"] ?= null
        params["api_keys"] ?= null
        params["limit"] ?= 100

        @master.call('messages/search', params, callback)

    ###
    Search the content of recently sent messages and return the aggregated hourly stats for matching messages
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} query the search terms to find matching messages for
    @option params {String} date_from start date
    @option params {String} date_to end date
    @option params {Array} tags an array of tag names to narrow the search to, will return messages that contain ANY of the tags
    @option params {Array} senders an array of sender addresses to narrow the search to, will return messages sent by ANY of the senders
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    searchTimeSeries: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["query"] ?= '*'
        params["date_from"] ?= null
        params["date_to"] ?= null
        params["tags"] ?= null
        params["senders"] ?= null

        @master.call('messages/search-time-series', params, callback)

    ###
    Get the information for a single recently sent message
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique id of the message to get - passed as the "_id" field in webhooks, send calls, or search calls
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('messages/info', params, callback)

    ###
    Get the full content of a recently sent message
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique id of the message to get - passed as the "_id" field in webhooks, send calls, or search calls
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    content: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('messages/content', params, callback)

    ###
    Parse the full MIME document for an email message, returning the content of the message broken into its constituent pieces
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} raw_message the full MIME document of an email message
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    parse: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('messages/parse', params, callback)

    ###
    Take a raw MIME document for a message, and send it exactly as if it were sent through Mandrill's SMTP servers
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} raw_message the full MIME document of an email message
    @option params {String|null} from_email optionally define the sender address - otherwise we'll use the address found in the provided headers
    @option params {String|null} from_name optionally define the sender alias
    @option params {Array|null} to optionally define the recipients to receive the message - otherwise we'll use the To, Cc, and Bcc headers provided in the document
         - to[] {String} the email address of the recipient
    @option params {Boolean} async enable a background sending mode that is optimized for bulk sending. In async mode, messages/sendRaw will immediately return a status of "queued" for every recipient. To handle rejections when sending in async mode, set up a webhook for the 'reject' event. Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are always sent asynchronously, regardless of the value of async.
    @option params {String} ip_pool the name of the dedicated ip pool that should be used to send the message. If you do not have any dedicated IPs, this parameter has no effect. If you specify a pool that does not exist, your default pool will be used instead.
    @option params {String} send_at when this message should be sent as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. If you specify a time in the past, the message will be sent immediately.
    @option params {String} return_path_domain a custom domain to use for the messages's return-path
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    sendRaw: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["from_email"] ?= null
        params["from_name"] ?= null
        params["to"] ?= null
        params["async"] ?= false
        params["ip_pool"] ?= null
        params["send_at"] ?= null
        params["return_path_domain"] ?= null

        @master.call('messages/send-raw', params, callback)

    ###
    Queries your scheduled emails by sender or recipient, or both.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} to an optional recipient address to restrict results to
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    listScheduled: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["to"] ?= null

        @master.call('messages/list-scheduled', params, callback)

    ###
    Cancels a scheduled email.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id a scheduled email id, as returned by any of the messages/send calls or messages/list-scheduled
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    cancelScheduled: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('messages/cancel-scheduled', params, callback)

    ###
    Reschedules a scheduled email.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id a scheduled email id, as returned by any of the messages/send calls or messages/list-scheduled
    @option params {String} send_at the new UTC timestamp when the message should sent. Mandrill can't time travel, so if you specify a time in past the message will be sent immediately
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    reschedule: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('messages/reschedule', params, callback)
