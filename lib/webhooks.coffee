module.exports = class Webhooks
    constructor: (@master) ->

    ###
    Get the list of all webhooks defined on the account
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('webhooks/list', params, callback)

    ###
    Add a new webhook
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} url the URL to POST batches of events
    @option params {String} description an optional description of the webhook
    @option params {Array} events an optional list of events that will be posted to the webhook
         - events[] {String} the individual event to listen for
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    add: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["description"] ?= null
        params["events"] ?= []

        @master.call('webhooks/add', params, callback)

    ###
    Given the ID of an existing webhook, return the data about it
    @param {Object} params the hash of the parameters to pass to the request
    @option params {Integer} id the unique identifier of a webhook belonging to this account
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('webhooks/info', params, callback)

    ###
    Update an existing webhook
    @param {Object} params the hash of the parameters to pass to the request
    @option params {Integer} id the unique identifier of a webhook belonging to this account
    @option params {String} url the URL to POST batches of events
    @option params {String} description an optional description of the webhook
    @option params {Array} events an optional list of events that will be posted to the webhook
         - events[] {String} the individual event to listen for
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    update: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["description"] ?= null
        params["events"] ?= []

        @master.call('webhooks/update', params, callback)

    ###
    Delete an existing webhook
    @param {Object} params the hash of the parameters to pass to the request
    @option params {Integer} id the unique identifier of a webhook belonging to this account
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('webhooks/delete', params, callback)
