module.exports = class Tags
    constructor: (@master) ->

    ###
    Return all of the user-defined tag information
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('tags/list', params, callback)

    ###
    Deletes a tag permanently. Deleting a tag removes the tag from any messages
    that have been sent, and also deletes the tag's stats. There is no way to
    undo this operation, so use it carefully.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} tag a tag name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('tags/delete', params, callback)

    ###
    Return more detailed information about a single tag, including aggregates of recent stats
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} tag an existing tag name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('tags/info', params, callback)

    ###
    Return the recent history (hourly stats for the last 30 days) for a tag
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} tag an existing tag name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    timeSeries: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('tags/time-series', params, callback)

    ###
    Return the recent history (hourly stats for the last 30 days) for all tags
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    allTimeSeries: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('tags/all-time-series', params, callback)
