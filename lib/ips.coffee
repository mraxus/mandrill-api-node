module.exports = class Ips
    constructor: (@master) ->

    ###
    Lists your dedicated IPs.
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/list', params, callback)

    ###
    Retrieves information about a single dedicated ip.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated IP address
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/info', params, callback)

    ###
    Requests an additional dedicated IP for your account. Accounts may
    have one outstanding request at any time, and provisioning requests
    are processed within 24 hours.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {Boolean} warmup whether to enable warmup mode for the ip
    @option params {String} pool the id of the pool to add the dedicated ip to, or null to use your account's default pool
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    provision: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["warmup"] ?= false
        params["pool"] ?= null

        @master.call('ips/provision', params, callback)

    ###
    Begins the warmup process for a dedicated IP. During the warmup process,
    Mandrill will gradually increase the percentage of your mail that is sent over
    the warming-up IP, over a period of roughly 30 days. The rest of your mail
    will be sent over shared IPs or other dedicated IPs in the same pool.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated ip address
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    startWarmup: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/start-warmup', params, callback)

    ###
    Cancels the warmup process for a dedicated IP.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated ip address
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    cancelWarmup: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/cancel-warmup', params, callback)

    ###
    Moves a dedicated IP to a different pool.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated ip address
    @option params {String} pool the name of the new pool to add the dedicated ip to
    @option params {Boolean} create_pool whether to create the pool if it does not exist; if false and the pool does not exist, an Unknown_Pool will be thrown.
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    setPool: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["create_pool"] ?= false

        @master.call('ips/set-pool', params, callback)

    ###
    Deletes a dedicated IP. This is permanent and cannot be undone.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip the dedicated ip to remove from your account
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/delete', params, callback)

    ###
    Lists your dedicated IP pools.
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    listPools: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/list-pools', params, callback)

    ###
    Describes a single dedicated IP pool.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} pool a pool name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    poolInfo: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/pool-info', params, callback)

    ###
    Creates a pool and returns it. If a pool already exists with this
    name, no action will be performed.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} pool the name of a pool to create
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    createPool: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/create-pool', params, callback)

    ###
    Deletes a pool. A pool must be empty before you can delete it, and you cannot delete your default pool.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} pool the name of the pool to delete
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    deletePool: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/delete-pool', params, callback)

    ###
    Tests whether a domain name is valid for use as the custom reverse
    DNS for a dedicated IP.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated ip address
    @option params {String} domain the domain name to test
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    checkCustomDns: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/check-custom-dns', params, callback)

    ###
    Configures the custom DNS name for a dedicated IP.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} ip a dedicated ip address
    @option params {String} domain a domain name to set as the dedicated IP's custom dns name.
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    setCustomDns: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('ips/set-custom-dns', params, callback)
