module.exports = class Metadata
    constructor: (@master) ->


    ###
    Get the list of custom metadata fields indexed for the account.
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} onsuccess an optional callback to execute when the API call is successfully made
    @param {Function} onerror an optional callback to execute when the API call errors out - defaults to throwing the error as an exception
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('metadata/list', params, callback)

    ###
    Add a new custom metadata field to be indexed for the account.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} name a unique identifier for the metadata field
    @option params {String} view_template optional Mustache template to control how the metadata is rendered in your activity log
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    add: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["view_template"] ?= null

        @master.call('metadata/add', params, callback)

    ###
    Update an existing custom metadata field.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} name the unique identifier of the metadata field to update
    @option params {String} view_template optional Mustache template to control how the metadata is rendered in your activity log
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    update: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('metadata/update', params, callback)

    ###
    Delete an existing custom metadata field. Deletion isn't instataneous, and /metadata/list will continue to return the field until the asynchronous deletion process is complete.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} name the unique identifier of the metadata field to update
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('metadata/delete', params, callback)
