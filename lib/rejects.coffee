module.exports = class Rejects
    constructor: (@master) ->

    ###
    Adds an email to your email rejection blacklist. Addresses that you
    add manually will never expire and there is no reputation penalty
    for removing them from your blacklist. Attempting to blacklist an
    address that has been whitelisted will have no effect.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email an email address to block
    @option params {String} comment an optional comment describing the rejection
    @option params {String} subaccount an optional unique identifier for the subaccount to limit the blacklist entry
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    add: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["comment"] ?= null
        params["subaccount"] ?= null

        @master.call('rejects/add', params, callback)

    ###
    Retrieves your email rejection blacklist. You can provide an email
    address to limit the results. Returns up to 1000 results. By default,
    entries that have expired are excluded from the results; set
    include_expired to true to include them.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email an optional email address to search by
    @option params {Boolean} include_expired whether to include rejections that have already expired.
    @option params {String} subaccount an optional unique identifier for the subaccount to limit the blacklist
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["email"] ?= null
        params["include_expired"] ?= false
        params["subaccount"] ?= null

        @master.call('rejects/list', params, callback)

    ###
    Deletes an email rejection. There is no limit to how many rejections
    you can remove from your blacklist, but keep in mind that each deletion
    has an affect on your reputation.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email an email address
    @option params {String} subaccount an optional unique identifier for the subaccount to limit the blacklist deletion
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["subaccount"] ?= null

        @master.call('rejects/delete', params, callback)
