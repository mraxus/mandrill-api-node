module.exports = class Urls
    constructor: (@master) ->

    ###
    Get the 100 most clicked URLs
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/list', params, callback)

    ###
    Return the 100 most clicked URLs that match the search query given
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} q a search query
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    search: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/search', params, callback)

    ###
    Return the recent history (hourly stats for the last 30 days) for a url
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} url an existing URL
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    timeSeries: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/time-series', params, callback)

    ###
    Get the list of tracking domains set up for this account
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    trackingDomains: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/tracking-domains', params, callback)

    ###
    Add a tracking domain to your account
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain a domain name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    addTrackingDomain: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/add-tracking-domain', params, callback)

    ###
    Checks the CNAME settings for a tracking domain. The domain must have been added already with the add-tracking-domain call
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain an existing tracking domain name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    checkTrackingDomain: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('urls/check-tracking-domain', params, callback)
