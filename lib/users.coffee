module.exports = class Users
    constructor: (@master) ->

    ###
    Return the information about the API-connected user
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('users/info', params, callback)

    ###
    Validate an API key and respond to a ping
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    ping: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('users/ping', params, callback)

    ###
    Validate an API key and respond to a ping (anal JSON parser version)
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    ping2: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('users/ping2', params, callback)

    ###
    Return the senders that have tried to use this account, both verified and unverified
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    senders: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        @master.call('users/senders', params, callback)
