module.exports = class Subaccounts
    constructor: (@master) ->

    ###
    Get the list of subaccounts defined for the account, optionally filtered by a prefix
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} q an optional prefix to filter the subaccounts' ids and names
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["q"] ?= null

        @master.call('subaccounts/list', params, callback)

    ###
    Add a new subaccount
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id a unique identifier for the subaccount to be used in sending calls
    @option params {String} name an optional display name to further identify the subaccount
    @option params {String} notes optional extra text to associate with the subaccount
    @option params {Integer} custom_quota an optional manual hourly quota for the subaccount. If not specified, Mandrill will manage this based on reputation
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    add: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["name"] ?= null
        params["notes"] ?= null
        params["custom_quota"] ?= null

        @master.call('subaccounts/add', params, callback)

    ###
    Given the ID of an existing subaccount, return the data about it
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of the subaccount to query
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    info: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('subaccounts/info', params, callback)

    ###
    Update an existing subaccount
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of the subaccount to update
    @option params {String} name an optional display name to further identify the subaccount
    @option params {String} notes optional extra text to associate with the subaccount
    @option params {Integer} custom_quota an optional manual hourly quota for the subaccount. If not specified, Mandrill will manage this based on reputation
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    update: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["name"] ?= null
        params["notes"] ?= null
        params["custom_quota"] ?= null

        @master.call('subaccounts/update', params, callback)

    ###
    Delete an existing subaccount. Any email related to the subaccount will be saved, but stats will be removed and any future sending calls to this subaccount will fail.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of the subaccount to delete
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('subaccounts/delete', params, callback)

    ###
    Pause a subaccount's sending. Any future emails delivered to this subaccount will be queued for a maximum of 3 days until the subaccount is resumed.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of the subaccount to pause
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    pause: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('subaccounts/pause', params, callback)

    ###
    Resume a paused subaccount's sending
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of the subaccount to resume
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    resume: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('subaccounts/resume', params, callback)
