// Generated by CoffeeScript 1.7.1
var Senders;

module.exports = Senders = (function() {
  function Senders(master) {
    this.master = master;
  }


  /*
  Return the senders that have tried to use this account.
  @param {Object} params the hash of the parameters to pass to the request
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.list = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/list', params, callback);
  };


  /*
  Returns the sender domains that have been added to this account.
  @param {Object} params the hash of the parameters to pass to the request
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.domains = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/domains', params, callback);
  };


  /*
  Adds a sender domain to your account. Sender domains are added automatically as you
  send, but you can use this call to add them ahead of time.
  @param {Object} params the hash of the parameters to pass to the request
  @option params {String} domain a domain name
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.addDomain = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/add-domain', params, callback);
  };


  /*
  Checks the SPF and DKIM settings for a domain. If you haven't already added this domain to your
  account, it will be added automatically.
  @param {Object} params the hash of the parameters to pass to the request
  @option params {String} domain a domain name
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.checkDomain = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/check-domain', params, callback);
  };


  /*
  Sends a verification email in order to verify ownership of a domain.
  Domain verification is an optional step to confirm ownership of a domain. Once a
  domain has been verified in a Mandrill account, other accounts may not have their
  messages signed by that domain unless they also verify the domain. This prevents
  other Mandrill accounts from sending mail signed by your domain.
  @param {Object} params the hash of the parameters to pass to the request
  @option params {String} domain a domain name at which you can receive email
  @option params {String} mailbox a mailbox at the domain where the verification email should be sent
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.verifyDomain = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/verify-domain', params, callback);
  };


  /*
  Return more detailed information about a single sender, including aggregates of recent stats
  @param {Object} params the hash of the parameters to pass to the request
  @option params {String} address the email address of the sender
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.info = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/info', params, callback);
  };


  /*
  Return the recent history (hourly stats for the last 30 days) for a sender
  @param {Object} params the hash of the parameters to pass to the request
  @option params {String} address the email address of the sender
  @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
   */

  Senders.prototype.timeSeries = function(params, callback) {
    if (params == null) {
      params = {};
    }
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    return this.master.call('senders/time-series', params, callback);
  };

  return Senders;

})();
