module.exports = class Whitelists
    constructor: (@master) ->

    ###
    Adds an email to your email rejection whitelist. If the address is
    currently on your blacklist, that blacklist entry will be removed
    automatically.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email an email address to add to the whitelist
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    add: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('whitelists/add', params, callback)

    ###
    Retrieves your email rejection whitelist. You can provide an email
    address or search prefix to limit the results. Returns up to 1000 results.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email an optional email address or prefix to search by
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    list: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["email"] ?= null

        @master.call('whitelists/list', params, callback)

    ###
    Removes an email address from the whitelist.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} email the email address to remove from the whitelist
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    delete: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('whitelists/delete', params, callback)
