module.exports = class Inbound
    constructor: (@master) ->

    ###
    List the domains that have been configured for inbound delivery
    @param {Object} params the hash of the parameters to pass to the request
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    domains: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/domains', params, callback)

    ###
    Add an inbound domain to your account
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain a domain name
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    addDomain: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/add-domain', params, callback)

    ###
    Check the MX settings for an inbound domain. The domain must have already been added with the add-domain call
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain an existing inbound domain
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    checkDomain: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/check-domain', params, callback)

    ###
    Delete an inbound domain from the account. All mail will stop routing for this domain immediately.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain an existing inbound domain
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    deleteDomain: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/delete-domain', params, callback)

    ###
    List the mailbox routes defined for an inbound domain
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain the domain to check
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    routes: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/routes', params, callback)

    ###
    Add a new mailbox route to an inbound domain
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} domain an existing inbound domain
    @option params {String} pattern the search pattern that the mailbox name should match
    @option params {String} url the webhook URL where the inbound messages will be published
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    addRoute: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/add-route', params, callback)

    ###
    Update the pattern or webhook of an existing inbound mailbox route. If null is provided for any fields, the values will remain unchanged.
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of an existing mailbox route
    @option params {String} pattern the search pattern that the mailbox name should match
    @option params {String} url the webhook URL where the inbound messages will be published
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    updateRoute: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["pattern"] ?= null
        params["url"] ?= null

        @master.call('inbound/update-route', params, callback)

    ###
    Delete an existing inbound mailbox route
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} id the unique identifier of an existing route
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    deleteRoute: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}


        @master.call('inbound/delete-route', params, callback)

    ###
    Take a raw MIME document destined for a domain with inbound domains set up, and send it to the inbound hook exactly as if it had been sent over SMTP
    @param {Object} params the hash of the parameters to pass to the request
    @option params {String} raw_message the full MIME document of an email message
    @option params {Array|null} to optionally define the recipients to receive the message - otherwise we'll use the To, Cc, and Bcc headers provided in the document
         - to[] {String} the email address of the recipient
    @option params {String} mail_from the address specified in the MAIL FROM stage of the SMTP conversation. Required for the SPF check.
    @option params {String} helo the identification provided by the client mta in the MTA state of the SMTP conversation. Required for the SPF check.
    @option params {String} client_address the remote MTA's ip address. Optional; required for the SPF check.
    @param {Function} callback an optional callback to execute when the API call returns - defaults to throwing errors as exceptions
    ###
    sendRaw: (params={}, callback) ->
        if typeof params == 'function'
            callback = params
            params = {}

        params["to"] ?= null
        params["mail_from"] ?= null
        params["helo"] ?= null
        params["client_address"] ?= null

        @master.call('inbound/send-raw', params, callback)
